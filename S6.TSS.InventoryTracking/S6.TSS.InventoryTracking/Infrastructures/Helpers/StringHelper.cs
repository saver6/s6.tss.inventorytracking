﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace S6.TSS.InventoryTracking.Infrastructures.Helpers
{
    public static class StringHelper
    {
        public static DbGeography ToDBGeography(this string str)
        {
            return DbGeography.PointFromText(str, 4326);
        }
        public static string ToSpacedWord(this string str)
        {
            return Regex.Replace(str, "([a-z](?=[A-Z])|[A-Z](?=[A-Z][a-z]))", "$1 ");
        }
        public static string ToBase64(this byte[] bytes)
        {
            return Convert.ToBase64String(bytes);
        }
        public static string NullToBlank(this string str)
        {
            if (str == null)
            {
                return "";
            }
            else
            {
                return str;
            }
        }
    }
}