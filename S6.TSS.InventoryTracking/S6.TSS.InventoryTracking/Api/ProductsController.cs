﻿using S6.TSS.InventoryTracking.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace S6.TSS.InventoryTracking.Api
{
    public class ProductsController
    {
        readonly IWarehouseService _warehouseService;
        readonly IProductItemService _productItemService;
        public ProductsController(IWarehouseService warehouseService, IProductItemService productItemService)
        {
            _warehouseService = warehouseService;
            _productItemService = productItemService;
        }

        // Action::View

        // Action::Create

        // Action::Update

        // Action::Destroy
    }
}