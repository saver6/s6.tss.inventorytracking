﻿using S6.TSS.InventoryTracking.Api.Base;
using S6.TSS.InventoryTracking.Api.Models.Warehouses;
using S6.TSS.InventoryTracking.Domain.Services;
using S6.TSS.InventoryTracking.Infrastructures.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace S6.TSS.InventoryTracking.Api
{
    public class WarehousesController : BaseApiController
    {
        readonly IWarehouseService _warehouseService;
        public WarehousesController(IWarehouseService warehouseService)
        {
            _warehouseService = warehouseService;
        }

        // Action::View
        [HttpGet, Route("api/warehouses/get-list")]
        public List<WarehouseDto> GetList()
        {
            return _warehouseService.All().Select(a => new WarehouseDto()
            {
                Address = a.Address,
                Name = a.Name,
                Location = a.Location
            }).ToList();
        }

        // Action::Create
        [HttpPost, Route("api/warehouses/new")]
        public ActionDto NewWarehouse(WarehouseDto data)
        {
            try
            {
                if (data.Latitude != null && data.Longitude != null)
                {
                    data.Location = string.Format("POINT({0} {1})", data.Longitude, data.Latitude.Value).ToDBGeography();
                }
                _warehouseService.AddNewWarehouse(new Domain.Models.Warehouse()
                {
                    Address = data.Address,
                    Name = data.Name,
                    Location = data.Location
                });
            }
            catch (Exception ex)
            {
                var err = new List<String>();
                err.Add(ex.Message);
                return new ActionDto().Error(err);
            }
            return new ActionDto().Success();
        }

        // Action::Update
        [HttpPost, Route("api/warehouses/edit")]
        public ActionDto UpdateWarehouse(WarehouseDto data)
        {
            try
            {
                var warehouse = _warehouseService.FindById(data.Id);
                if (data.Latitude != null && data.Longitude != null)
                {
                    warehouse.Location = string.Format("POINT({0} {1})", data.Longitude, data.Latitude.Value).ToDBGeography();
                }
                warehouse.Name = data.Name;
                warehouse.Address = data.Address;
                _warehouseService.Edit(warehouse);
                _warehouseService.Save();
            }
            catch (Exception ex)
            {
                var err = new List<String>();
                err.Add(ex.Message);
                return new ActionDto().Error(err);
            }
            return new ActionDto().Success();
        }

        // Action::Destroy

    }
}