﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Web;

namespace S6.TSS.InventoryTracking.Api.Models.Warehouses
{
    public class WarehouseDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public DbGeography Location { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
    }
}