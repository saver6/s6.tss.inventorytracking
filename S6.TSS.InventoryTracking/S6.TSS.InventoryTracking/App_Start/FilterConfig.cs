﻿using System.Web;
using System.Web.Mvc;

namespace S6.TSS.InventoryTracking
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
