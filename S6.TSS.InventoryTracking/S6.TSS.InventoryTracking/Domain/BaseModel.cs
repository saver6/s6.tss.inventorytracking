﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace S6.TSS.InventoryTracking.Domain
{
    public class BaseModel
    {
        public BaseModel()
        {
            this.DateCreated = DateTimeOffset.UtcNow;
            this.DateUpdated = DateTimeOffset.UtcNow;
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public DateTimeOffset DateCreated { get; set; }

        public DateTimeOffset DateUpdated { get; set; }
    }
}