﻿using S6.TSS.InventoryTracking.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace S6.TSS.InventoryTracking.Domain.Services
{
    public class ProductItemService : BaseService<DefaultDbContext, ProductItem>, IProductItemService
    {
    }

    public interface IProductItemService : IBaseService<ProductItem>
    {
    }
}