﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace S6.TSS.InventoryTracking.Domain.Models
{
    public class OrderItem : BaseModel
    {
        public Guid OrderId { get; set; }
        public Guid ProductItemId { get; set; }
        public double Quantity { get; set; }
    }
}