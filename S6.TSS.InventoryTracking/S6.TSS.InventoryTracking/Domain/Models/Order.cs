﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace S6.TSS.InventoryTracking.Domain.Models
{
    public class Order : BaseModel
    {
        public string Description { get; set; }
        public string Buyer { get; set; }
        public OrderStatus Status { get; set; }
    }
    public enum OrderStatus
    {
        Processing,
        OnHold,
        Closed,
        Canceled,
        Complete
    }
}