﻿using S6.TSS.InventoryTracking.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace S6.TSS.InventoryTracking.Domain
{
    public class DefaultDbContext : DbContext
    {

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            base.OnModelCreating(modelBuilder);
        }


        public DefaultDbContext()
            : base("name=DefaultDbContext")
        {
            //Database.SetInitializer<DefaultDbContext>(null);
            Database.SetInitializer(new CreateDatabaseIfNotExists<DefaultDbContext>());
        }

        public virtual DbSet<Warehouse> Warehouses { get; set; }
        public virtual DbSet<ProductItem> ProductItems { get; set; }
        public virtual DbSet<WarehouseProductItem> WarehouseProductItems { get; set; }
        public virtual DbSet<Truck> Trucks { get; set; }
        public virtual DbSet<Parcel> Parcels { get; set; }
        public virtual DbSet<DeliveryRequest> DeliveryRequests { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderItem> OrderItems { get; set; }
        public virtual DbSet<WarehouseMovement> WarehouseMovements { get; set; }
    }
}