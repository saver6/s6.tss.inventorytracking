﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace S6.TSS.InventoryTracking.Domain.Models
{
    public class DeliveryRequest : BaseModel
    {
        public Guid ParcelId { get; set; }
        public Guid TruckId { get; set; }
        public Guid OrderId { get; set; }
        public DeliveryStatus Status { get; set; }
    }
    public enum DeliveryStatus
    {
        Accepted,
        InTransit,
        Delivered
    }
}