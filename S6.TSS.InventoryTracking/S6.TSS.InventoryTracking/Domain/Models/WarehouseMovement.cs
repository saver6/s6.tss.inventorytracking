﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace S6.TSS.InventoryTracking.Domain.Models
{
    public class WarehouseMovement : BaseModel
    {
        public Guid WarehouseProductItemId { get; set; }
        public double Quantity { get; set; } // Increment / Decrement (+/-)
        public string Reason { get; set; } // Add Order/Reference ID
    }
}