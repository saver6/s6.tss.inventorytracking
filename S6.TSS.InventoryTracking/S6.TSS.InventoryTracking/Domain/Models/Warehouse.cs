﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Web;

namespace S6.TSS.InventoryTracking.Domain.Models
{
    public class Warehouse : BaseModel
    {
        public string Name { get; set; }
        public DbGeography Location { get; set; }
        public string Address { get; set; }
    }
}