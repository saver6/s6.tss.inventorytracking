﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;

namespace S6.TSS.InventoryTracking.Api.Base
{
    public class BaseApiController : ApiController
    {
        public Guid? ClientId
        {
            get
            {
                try
                {
                    return Guid.Parse(Request.Headers.GetValues("ClientId").FirstOrDefault().ToString());
                }
                catch (Exception e)
                {
                    return null;
                }
            }
        }
    }
    public class ActionDto
    {
        public List<string> Message { get; set; }
        public HttpStatusCode Status { get; set; }
        public ActionDto Success()
        {
            return new ActionDto()
            {
                Message = new List<string>() { "Ok" },
                Status = HttpStatusCode.OK
            };

        }

        public ActionDto Error(List<String> messages)
        {
            return new ActionDto()
            {
                Status = HttpStatusCode.BadRequest,
                Message = messages
            };
        }
    }
}