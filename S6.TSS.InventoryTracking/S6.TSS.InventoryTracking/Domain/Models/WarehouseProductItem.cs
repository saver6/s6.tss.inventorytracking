﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace S6.TSS.InventoryTracking.Domain.Models
{
    public class WarehouseProductItem : BaseModel
    {
        public Guid WarehouseProductItemId { get; set; }
        public Guid ProductItem { get; set; }
        public double Quantity { get; set; }
        public string ReferenceId { get; set; }
    }
}