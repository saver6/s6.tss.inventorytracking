using Microsoft.Practices.Unity;
using S6.TSS.InventoryTracking.Domain.Services;
using System.Web.Http;
using System.Web.Mvc;
using Unity.Mvc5;

namespace S6.TSS.InventoryTracking
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();

            container.RegisterType<IWarehouseService, WarehouseService>();
            container.RegisterType<IProductItemService, ProductItemService>();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));

            GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);
        }
    }
}