﻿using S6.TSS.InventoryTracking.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace S6.TSS.InventoryTracking.Domain.Services
{
    public class WarehouseService : BaseService<DefaultDbContext, Warehouse>, IWarehouseService
    {
        public void AddNewWarehouse(Warehouse warehouse)
        {
            this.Context.Set<Warehouse>().Add(warehouse);
            this.Context.SaveChanges();
        }
    }

    public interface IWarehouseService : IBaseService<Warehouse>
    {
        void AddNewWarehouse(Warehouse warehouse);
    }
}