﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace S6.TSS.InventoryTracking.Domain.Models
{
    public class Truck : BaseModel
    {
        public string Driver { get; set; }
        public string Description { get; set; }
    }
}