﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace S6.TSS.InventoryTracking.Domain.Models
{
    public class ProductItem : BaseModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}